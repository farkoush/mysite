<?php
function homa_resources()
{
  wp_enqueue_style(style,get_stylesheet_uri());
}
add_action( 'wp_enqueue_scripts', 'homa_resources' );

//Navigation Menu
function register_my_menus() {
register_nav_menus(array(
    'primary'=>__('primary menu'),
    'footer'=>__('footer menu')
  )
);
}
add_action( 'init', 'register_my_menus' );

?>
